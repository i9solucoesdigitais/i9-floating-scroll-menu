<?php
/*
Plugin Name: i9 Floating Scroll Menu
Plugin URI: http://i9solucoesdigitais.com.br
Description: A WordPress menu that is initially hidden and shows up on scroll down.
Version: 0.2
Author: i9 Soluções Digitais
Author URI: http://i9solucoesdigitais.com.br
Text Domain: i9fsm
Domain Path: /languages
License: GPL2
*/

if ( ! defined( 'ABSPATH' ) ) die( 'Error! Please contact the administrator.' );

class i9FloatingScrollMenu {
	private $textDomain = 'i9fsm';
	private $wpMainMenuLocation = 'i9fsmMenu';
	private $wpHomeSectionMenuLocation = 'i9fsmHomeSectionMenu';

	function __construct() {
		add_action( 'plugins_loaded', array( $this, 'loadTextDomain' ) );
		add_action( 'init', array( $this, 'registerNewMenu' ) );
		add_shortcode( 'floating-scroll-menu', array( $this, 'printMainMenuShortcode' ) );
		add_shortcode( 'home-section-menu', array( $this, 'printHomeSectionMenuShortcode' ) );

		if ( is_admin() ) {
            include( 'includes/admin/settings.php' ); // Settings
        }
	}

	public function loadTextDomain() {
	    $locale = apply_filters( 'plugin_locale', get_locale(), $this->textDomain );

		load_textdomain( $this->textDomain, WP_LANG_DIR . '/i9-floating-scroll-menu/' . $this->textDomain . '-'. $locale . '.mo' );
		load_plugin_textdomain( $this->textDomain, false, dirname( plugin_basename(__FILE__) ) . '/languages/' );
	}

	public function registerNewMenu() {
		register_nav_menu( $this->wpMainMenuLocation, __( 'i9 Floating Scroll Menu' ));

		register_nav_menu( $this->wpHomeSectionMenuLocation, __( 'Home Section Menu' ));		
	}

	public function printMainMenuShortcode() {
		wp_enqueue_style( 'i9fsm-style', plugin_dir_url( __FILE__ ) . 'includes/css/style.css' );
		wp_enqueue_script( 'i9fsm-script', plugin_dir_url( __FILE__ ).'includes/js/min/script-min.js', array( 'jquery' ) );
	    wp_enqueue_script( 'jquery' );

    	$menuArgs = array(
    		'container' => 'nav',
    		'container_id' => 'fsm-main',
    		'container_class' => 'clearfix',
    		'menu_class' => 'clearfix',
    		'theme_location' => $this->wpMainMenuLocation
    	);

    	$i9fsmSettings = get_option( 'i9fsmSettings' );

    	$facebookAccount = $i9fsmSettings['facebookAccount'];
    	$googlePlusAccount = $i9fsmSettings['googlePlusAccount'];
    	$twitterAccount = $i9fsmSettings['twitterAccount'];
    	$linkedinAccount = $i9fsmSettings['linkedinAccount'];
    	$instagramAccount = $i9fsmSettings['instagramAccount'];
    	$youtubeAccount = $i9fsmSettings['youtubeAccount'];

		ob_start();
		?>
			<div id="i9-fsmenu" class="menu-hidden<?php echo is_admin_bar_showing() ? ' add-top-css' : ''; ?>">

				<?php if ( has_nav_menu( $this->wpMainMenuLocation ) ) : ?>

				<div class="menu-container clearfix">
					<?php wp_nav_menu( $menuArgs ); ?>

					<div id="fsm-social-menu">
						<ul class="clearfix">
							<?php if ( !empty( $facebookAccount ) ) : ?>
								<li><a href="<?php echo $facebookAccount; ?>" target="_blank"><i class="fa fa-facebook fa-fw"></i></a></li>
							<?php endif; ?>
							<?php if ( !empty( $googlePlusAccount ) ) : ?>
								<li><a href="<?php echo $googlePlusAccount; ?>" target="_blank"><i class="fa fa-google-plus fa-fw"></i></a></li>
							<?php endif; ?>
							<?php if ( !empty( $twitterAccount ) ) : ?>
								<li><a href="<?php echo $twitterAccount; ?>" target="_blank"><i class="fa fa-twitter fa-fw"></i></a></li>
							<?php endif; ?>
							<?php if ( !empty( $linkedinAccount ) ) : ?>
								<li><a href="<?php echo $linkedinAccount; ?>" target="_blank"><i class="fa fa-linkedin fa-fw"></i></a></li>
							<?php endif; ?>
							<?php if ( !empty( $instagramAccount ) ) : ?>
								<li><a href="<?php echo $instagramAccount; ?>" target="_blank"><i class="fa fa-instagram fa-fw"></i></a></li>
							<?php endif; ?>
							<?php if ( !empty( $youtubeAccount ) ) : ?>
								<li><a href="<?php echo $youtubeAccount; ?>" target="_blank"><i class="fa fa-youtube fa-fw"></i></a></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
				<?php else : ?>
	
				<div id="fsm-no-menu">
				<p><i class="fa fa-exclamation-triangle"></i> <?php _e( 'No menu assigned! Please select an menu.', 'i9fsm' ); ?></p>
				<?php
					if ( current_user_can( 'edit_theme_options' ) ) {
						$wpMenusURL = admin_url() . 'nav-menus.php';
						?><p class="menu-link"><a href="<?php echo $wpMenusURL; ?>"><?php _e( 'Click here to edit', 'i9fsm' ); ?> <i class="fa fa-edit"></i></a></p><?php
					}
				?>
				</div>

				<?php endif; ?>
			</div>

		<?php
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	public function printHomeSectionMenuShortcode() {
		$menuArgs = array(
    		'container' => 'div',
    		'container_id' => 'i9fsHomeSectionMenu',
    		'container_class' => 'clearfix',
    		'menu_class' => 'clearfix',
    		'theme_location' => $this->wpHomeSectionMenuLocation
    	);
		ob_start();
		?>
			<?php wp_nav_menu( $menuArgs ); ?>

		<?php
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}
}

new i9FloatingScrollMenu;