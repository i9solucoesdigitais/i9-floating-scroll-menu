<?php
add_action( 'admin_menu', 'i9fsmAddAdminMenu' );
add_action( 'admin_init', 'i9fsmSettingsInit' );

function i9fsmAddAdminMenu(  ) { 
	add_menu_page( 'i9 Floating Scroll Menu', __( 'Menu Settings', 'i9fsm' ), 'manage_options', 'i9fsmMenuSettings', 'i9fsmCreateSettingsPage', 'dashicons-menu' );
}

function i9fsmSettingsExist( ) { 
	if ( false == get_option( 'i9fsmSettings' ) ) { 
		add_option( 'i9fsmSettings' );
	}
}

function i9fsmSettingsInit( ) { 
	register_setting( 'i9fsmSettingsPage', 'i9fsmSettings', 'i9fsmSanitizeFields' );

	add_settings_section(
		'i9fsmSocialNetworkSection', 
		__( 'Social Networks', 'i9fsm' ), 
		'i9fsmSocialNetworkSectionDescription', 
		'i9fsmSettingsPage'
	);

	add_settings_field( 
		'facebookAccount',
		__( 'Facebook', 'i9fsm' ), 
		'i9fsmPrintFacebookAccountField', 
		'i9fsmSettingsPage', 
		'i9fsmSocialNetworkSection' 
	);
	add_settings_field( 
		'googlePlusAccount',
		__( 'Google+', 'i9fsm' ), 
		'i9fsmPrintGooglePlusAccountField', 
		'i9fsmSettingsPage', 
		'i9fsmSocialNetworkSection' 
	);
	add_settings_field( 
		'twitterAccount',
		__( 'Twitter', 'i9fsm' ), 
		'i9fsmPrintTwitterAccountField', 
		'i9fsmSettingsPage', 
		'i9fsmSocialNetworkSection' 
	);
	add_settings_field( 
		'linkedinAccount',
		__( 'Linked In', 'i9fsm' ), 
		'i9fsmPrintLinkedinAccountField', 
		'i9fsmSettingsPage', 
		'i9fsmSocialNetworkSection' 
	);
	add_settings_field( 
		'instagramAccount',
		__( 'Instagram', 'i9fsm' ), 
		'i9fsmPrintInstagramAccountField', 
		'i9fsmSettingsPage', 
		'i9fsmSocialNetworkSection' 
	);
	add_settings_field( 
		'youtubeAccount',
		__( 'YouTube', 'i9fsm' ), 
		'i9fsmPrintYouTubeAccountField', 
		'i9fsmSettingsPage', 
		'i9fsmSocialNetworkSection' 
	);
}


function i9fsmCreateSettingsPage(  ) { 
	?>
	<form action='options.php' method='post'>	
		<h1><?php _e( 'i9 Floating Scroll Menu Settings', 'i9fsm'); ?></h1>
		
		<?php
		settings_fields( 'i9fsmSettingsPage' );
		do_settings_sections( 'i9fsmSettingsPage' );
		submit_button();
		?>	
	</form>
	<?php
}

function i9fsmSanitizeFields( $input ) {
    $output = array();
     
    // Loop em cada entrada do formulário
    foreach ( $input as $key => $value ) {
         
        // Verifica e processa se exister valor para a entrada.
        if ( isset( $input[$key] ) ) {
         
            // Remove todas as tags PHP e HTML e limpa a entrada.
            $output[$key] = strip_tags( stripslashes( $input[ $key ] ) );
        } 
    }
    // Retorna o array processando qualquer funcao adicional filtrada por esta action
    return apply_filters( 'i9fsmSanitizeFields', $output, $entrada );
}

function i9fsmSocialNetworkSectionDescription(  ) { 
	echo __( 'Leave blank the networks that you don\'t want do display', 'i9fsm' );
}

function i9fsmPrintFacebookAccountField(  ) { 
	$options = get_option( 'i9fsmSettings' );
	?>
	<input type='text' name='i9fsmSettings[facebookAccount]' value='<?php echo $options['facebookAccount']; ?>'>
	<?php
}
function i9fsmPrintGooglePlusAccountField(  ) { 
	$options = get_option( 'i9fsmSettings' );
	?>
	<input type='text' name='i9fsmSettings[googlePlusAccount]' value='<?php echo $options['googlePlusAccount']; ?>'>
	<?php
}
function i9fsmPrintTwitterAccountField(  ) { 
	$options = get_option( 'i9fsmSettings' );
	?>
	<input type='text' name='i9fsmSettings[twitterAccount]' value='<?php echo $options['twitterAccount']; ?>'>
	<?php
}
function i9fsmPrintLinkedinAccountField(  ) { 
	$options = get_option( 'i9fsmSettings' );
	?>
	<input type='text' name='i9fsmSettings[linkedinAccount]' value='<?php echo $options['linkedinAccount']; ?>'>
	<?php
}
function i9fsmPrintInstagramAccountField(  ) { 
	$options = get_option( 'i9fsmSettings' );
	?>
	<input type='text' name='i9fsmSettings[instagramAccount]' value='<?php echo $options['instagramAccount']; ?>'>
	<?php
}
function i9fsmPrintYouTubeAccountField(  ) { 
	$options = get_option( 'i9fsmSettings' );
	?>
	<input type='text' name='i9fsmSettings[youtubeAccount]' value='<?php echo $options['youtubeAccount']; ?>'>
	<?php
}
