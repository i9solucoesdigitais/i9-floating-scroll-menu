jQuery(document).ready(function($) {
	var didScroll;

	// on scroll, let the interval function know the user has scrolled
	$(window).scroll(function() {
	  didScroll = true;
	});
	// run hasScrolled() and reset didScroll status
	setInterval(function() {
	  	if (didScroll) {
	    	hasScrolled();
	    	didScroll = false;
	  	}
	}, 250);
	function hasScrolled() {
		var menu = $('#i9-fsmenu');
		var st = $(this).scrollTop();

		var startPoint = $(window).height() * 0.6;

		if ($(this).scrollTop() > startPoint) {
			menu.removeClass('menu-hidden');
		}
		else {
			menu.addClass('menu-hidden');
		}
	}

});